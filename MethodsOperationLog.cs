﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;

namespace Project.Common
{
    public static class MethodsOperationLog
    {
        private delegate void timer_delegate();
        private static ConcurrentDictionary<int, StringBuilder> buffer = new ConcurrentDictionary<int, StringBuilder>();
        private static string dirMain = @"C:\DEBUG\";
        private static string cfgPatch;

        static MethodsOperationLog()
        {
            if( ! Directory.Exists(dirMain))    Directory.CreateDirectory(dirMain);

            cfgPatch = $"{dirMain}log.cfg";

            if( ! File.Exists(cfgPatch))
                using(StreamWriter file = new StreamWriter(cfgPatch, true, Encoding.GetEncoding(65001)))
                    file.Write(@"state ""0""");

            timer_delegate timer = timer_func;

            timer.BeginInvoke(null, null);
        }

        public static void set_state(string cvar_value_trim) => Environment.SetEnvironmentVariable("log_state", cvar_value_trim);
        public static int get_state() => byte.TryParse(Environment.GetEnvironmentVariable("log_state"), out byte state) ? state : 0;

        public static void timer_func()
        {
            Thread.Sleep(32); // 200 -> 10+, 100 -> 7-, 50 -> 5+, 25 -> 5+, 32 -> 5- минут на запись около 250 Мб.

            read_cfg();

            timer_func();
        }

        public static void log_to_buffer(string path_file, int line_file, int inserts_before, int insert_lines)
        {
            string state = Environment.GetEnvironmentVariable("log_state");
            
            if(is_int_positive(state))
                switch(Convert.ToInt32(state))
                {
                    case 1:
                    {
                        Process       currentProcess  = Process.GetCurrentProcess();
                        int           threadId        = Thread.CurrentThread.ManagedThreadId;
                        long          current_time    = (long)(DateTime.Now             - new DateTime(1970, 1, 1)).TotalMilliseconds,
                                      start_time_prog = (long)(currentProcess.StartTime - new DateTime(1970, 1, 1)).TotalMilliseconds;
                        StringBuilder msg_add         = new StringBuilder();

                        msg_add.Append(
                                (current_time - start_time_prog)           .ToString()   ); msg_add.Append( '\n' ); msg_add.Append(
                                 path_file                                               ); msg_add.Append( '\n' ); msg_add.Append(
                                (line_file - inserts_before * insert_lines).ToString()   ); msg_add.Append( '\n' ); msg_add.Append(
                                 line_file.ToString()                                    ); msg_add.Append( '\n' );

                        if(!buffer.ContainsKey(threadId))   buffer .TryAdd   (threadId,     msg_add);
                        else
                        {
                            StringBuilder msg_old                              ,
                                          msg_new         = new StringBuilder();

                                                  msg_old = buffer .GetOrAdd (threadId,     msg_new);
                                                            msg_new.Append                 (msg_old.Append(msg_add));
                                                            buffer .TryUpdate(threadId,     msg_new,       msg_old) ;
                        }

                        break;
                    }
                }
        }

        public static void write_files()
        {
            string dirProcess = $@"{dirMain}{Process.GetCurrentProcess().ProcessName}\";
            if( ! Directory.Exists(dirProcess)) Directory.CreateDirectory(dirProcess);

            ICollection<int>        threadIdCollection = buffer.Keys;
            foreach(int threadId in threadIdCollection)
            {
                string             path_log = $"{dirProcess}{threadId}.txt";
                StringBuilder      msg_old  = buffer.GetOrAdd (threadId, new StringBuilder());

                using(StreamWriter file     = new StreamWriter(path_log, true, Encoding.GetEncoding(65001)))
                {
                                   file      .Write                  (msg_old);
                                   file      .Flush();
                }
                                   buffer    .TryRemove(threadId, out msg_old);
            }
        }

        public static void read_cfg()
        {
            string        str            ,
                          cvar_value_trim;
            StringBuilder cvar_name ,
                          cvar_value;

            try
            {
                using(StreamReader file = new StreamReader(cfgPatch, Encoding.GetEncoding(65001)))
                {
                    while(!file.EndOfStream)
                    {
                        str = file.ReadLine();

                        parse_cvar(str, out cvar_name, out cvar_value);

                        if(cvar_name.Equals(new StringBuilder("state")))
                        {
                            cvar_value_trim = cvar_value.ToString().Trim('"');

                            if(is_int_positive(cvar_value_trim))
                                switch(Convert.ToInt32(cvar_value_trim))
                                {
                                    case 0:       { set_state(cvar_value_trim); break; }
                                 // case 1:       { set_state(cvar_value_trim); break; }
                                 // case 2:
                                    case 1:
                                    {
                                                    set_state(cvar_value_trim);
                                                    write_files();
                                                    break;
                                    }
                                }
                        }
                    }

                    file.Close();
                }
            }
            catch
            {
            }
        }
        
        public static bool is_int_positive(string str)
        {
            if(null == str)                         return false;

            string max_int      = "2147483647";
            int len_str         = str.Length,
                len_max_int     = max_int.Length,
                c;

            if(len_str == 0)                        return false;

            if(len_str > 1)
                if(str[0] == '0')                   return false;

            for(c = 0; c < len_str; ++ c)
                if( ! char.IsDigit(str[c]))         return false;

            if(len_str > len_max_int)               return false;
            if(len_str < len_max_int)               return true;

            for(c = 0; c < len_str; ++ c)
            {
                if(max_int[c] < str[c])             return false;
                if(max_int[c] > str[c])             return true;
            }

                                                    return true;
        }

        public static int parse_cvar(string str, out StringBuilder cvar_name, out StringBuilder cvar_value, int LEN_CVAR_NAME = 0, int LEN_CVAR_VALUE = 0)
        {
                                                                                cvar_name  = new StringBuilder();
                                                                                cvar_value = new StringBuilder();

                                                                                bool dest_switch = false,
                                                                                     quote       = false,
                                                                                     screen      = false;
                                                                                int  len_str     = str.Length,
                                                                                     i           = -1        ;
                                                                                char currentChar;

            while(++ i < len_str)
            {
                if(str[i] == ' '
                || str[i] == '\t')
                    if(!dest_switch)                                          { dest_switch = true;
                                                                                i ++;
                                                                                continue; }
                    else
                        if(cvar_value.Length == 0)                            { i ++;
                                                                                continue; }
                        else
                            if(!quote)                                          break;

                                                                                currentChar = str[i];

                if(!dest_switch)
                {
                    if( ! ('A' <= currentChar
                    &&            currentChar <= 'Z'
                    ||     'a' <= currentChar
                    &&            currentChar <= 'z'
                    ||     '0' <= currentChar
                    &&            currentChar <= '9'
                    ||            currentChar == '_'))                          return 2;
                }

                if(currentChar == '"')
                    if(!quote)
                    {
                        if(cvar_value.Length == 0)                              quote = true;;
                    }
                    else
                        if(!screen)
                        {
                            if(                         LEN_CVAR_VALUE > 0
                            && cvar_value.Length + 1 >= LEN_CVAR_VALUE)         return 6;
                                                                                cvar_value.Append('"');
                                                                                break;
                        }

                if(currentChar == '\\')                                         screen = true ;
                else                                                            screen = false;

                if(!dest_switch)
                {
                    if(                         LEN_CVAR_NAME > 0
                    && cvar_name .Length + 1 >= LEN_CVAR_NAME)                  return 4;
                                                                                cvar_name .Append(currentChar);
                }
                else
                {
                    if(                         LEN_CVAR_VALUE > 0
                    && cvar_value.Length + 1 >= LEN_CVAR_VALUE)                 return 6;
                                                                                cvar_value.Append(currentChar);
                }
            }

            if(cvar_name.Length == 0)                                           return 3;
    
                                                                                char firstChar = cvar_name[0];
            if( ! ('A' <= firstChar
            &&            firstChar <= 'Z'
            ||     'a' <= firstChar 
            &&            firstChar <= 'z'
            ||            firstChar == '_' ))                                   return 1;

            if(quote)
            {
                if(cvar_value.Length < 2
                || cvar_value[cvar_value.Length - 1] != '"')                    return 7;
            }
            else
                if(cvar_value.Length == 0)                                      return 5;
                                                                                /*
                                                                                cvar_name .Append('\0');
                                                                                cvar_value.Append('\0');
                                                                                */
                                                                                return 0;
        }
    }
}
