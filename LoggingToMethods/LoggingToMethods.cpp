#include "stdafx.h"

using namespace std;

void   list_dir             (const string & path, int level);
string get_file_extension   (string file_name);
string get_string_reverse   (string extension);

void   process_file         (string file_path, int & inserts);
void   offset_string_left   (char str[], int len, int offset = 1);
bool   is_close_quote       (string begin);

void   init_check_exceptions();
int         check_exceptions(const char str[]);
bool   is_where_generic     (string new_code);
string get_insert           (string file_path, int line, int inserts);
int    get_count_lines      (string insert);

void   init_str             (char str1[],const char str2[],int len_str1,int len_str2);
int    get_len              (const char str[]);
bool   is_equali            (const char str1[], const char str2[], const int len_str1, const int len_str2, int start_str1, int start_str2);
char   to_lower             (char ch);

const int LEN_BUFFER = 10;

int
    _files_all     = 0,
    _files_changed = 0,
    _inserts_total = 0;

int _tmain(int argc, _TCHAR * argv[])
{
    if(argc < 2)
    {
        cout
            << "\tError!" << endl
            << "\t\tCommand line arguments required:" << endl
            << "\t\t\tProject directory for logging." << endl
            << "\t\tCreate shortcut, right click, properties, object, add." << endl;
        return 1;
    }

    time_t begin = time(NULL);

    wstring ws(argv[1]);
    string dir(ws.begin(), ws.end());
    replace(dir.begin(), dir.end(), '\\', '/');

    init_check_exceptions();

    list_dir(dir, 0);

    time_t end   = time(NULL);

    cout
        << endl
        << "Time spent: ["
        << to_string(end - begin)
        << "] (seconds)"
        << endl
        << "Changed: ["
        << to_string(_files_changed)
        << "] (files)"
        << endl
        << "Total inserts: ["
        << to_string(_inserts_total)
        << "] (code)"
        << endl;

    system("pause");
}

void list_dir(const string & dir, int level)
{
    DIR    * dir_ptr = opendir(dir.c_str());
    dirent * entry;
    string   name    = "";
    int      inserts = 0 ;

    if(!dir_ptr)
    {
        perror("opendir");
        system("pause");
        exit  (1);
    };

    while((entry = readdir(dir_ptr)))
    {
        name = entry->d_name;

        if(name != "."
        && name != "..")
            switch(entry -> d_type)
            {
                case DT_DIR: // directories
                {
                    name = dir + "/" + entry->d_name;
                    list_dir(name, level + 1);

                    break;
                }

                case DT_REG: // files
                {
                    _files_all ++;

                    if(get_file_extension(name) == "cs")
                    {
                        process_file(dir + "/" + name, inserts);

                        if(inserts)
                        {
                            _files_changed ++;
                            _inserts_total += inserts;
                        }

                        cout << _files_all << '\t' << inserts << '\t' << dir + "/" + name << endl; // Прогресс.
                    }

                    break;
                }
            }
    }

    closedir(dir_ptr);
}

string get_file_extension(string file_name)
{
    string result = "";

    char cache = '\0';

    for(int s = file_name.length() - 1; s >= 0; -- s)
    {
        if((cache = file_name[s]) == '.')                   break;

        result.push_back(cache);
    }

    return get_string_reverse(result);
}

string get_string_reverse(string str)
{
    string temp = "";

    for(int s = str.length() - 1; s >= 0; -- s)     temp.push_back(str[s]);

    str = temp;

    return str;
}

// ------------------------------------------------------------------------------------------------------------------------------------------------------------

void process_file(string file_path, int & inserts)
{
    inserts = 0;

    FILE * f = NULL ;
    char   c = '\0';

    int lines = 1;

    string new_code = "",
           insert   = "";

    char buffer[LEN_BUFFER + 1];
    for(int c = 0; c <= LEN_BUFFER; ++ c)                   buffer[c] = '\0';

    bool comment_str          = false,
         comment_all          = false,
         quote_dog            = false,
         quote2               = false,
         quote1               = false,
         brackets_round_close = false,
         brackets_figure_open = false,
         exception            = false,
         skip_iteration       = false,
         where_generic_state  = false;

    int round_cnt  = 0,
        round_last = 0;

    if((f = fopen(file_path.c_str(), "rt")))
    {
        while(true)
        {
            c = fgetc(f);

            if(feof(f))                                     break;

            if(c == '\n')                                   lines ++;

            new_code.push_back(c);

            offset_string_left(buffer, sizeof(buffer) / sizeof(char), 1);
            buffer[LEN_BUFFER - 1] = c;

            if( comment_str
            && !comment_all
            && !quote2
            && !quote1
            &&  buffer[LEN_BUFFER - 1] == '\n')           { comment_str          = false;   continue; }

            if(
               !comment_str
            && !comment_all
            && !quote2
            && !quote1
            &&( 
                buffer[LEN_BUFFER - 1] == '#'
            ||
                buffer[LEN_BUFFER - 1] == '/'
            &&  buffer[LEN_BUFFER - 2] == '/'
            &&  buffer[LEN_BUFFER - 3] != '*'))             comment_str          = true;

            if(!comment_str
            &&  comment_all
            && !quote2
            && !quote1
            &&  buffer[LEN_BUFFER - 2] == '*'
            &&  buffer[LEN_BUFFER - 1] == '/')            { comment_all          = false;   continue; }

            if(!comment_str
            && !comment_all
            && !quote2
            && !quote1
            &&  buffer[LEN_BUFFER - 2] == '/'
            &&  buffer[LEN_BUFFER - 1] == '*')              comment_all          = true;

            if(!comment_str
            && !comment_all
            && !quote1)
                if(buffer[LEN_BUFFER - 1] == '"')
                    if(!quote2)
                    {
                        if(buffer[LEN_BUFFER - 2] == '@')   quote_dog            = true;
                                                            quote2               = true;
                    }
                    else
                        if(quote_dog)
                        {
                                                            quote_dog            = false;
                                                            quote2               = false;   continue;
                        }
                        else
                            if(is_close_quote(new_code))  { quote2               = false;   continue; }

            if(!comment_str
            && !comment_all
            && !quote2)
                if(buffer[LEN_BUFFER - 1] == '\'')
                    if(!quote1)                             quote1               = true;
                    else
                        if(buffer[LEN_BUFFER - 2] != '\\'){ quote1               = false;   continue; }

            if(comment_str
            || comment_all
            || quote2
            || quote1)                                                                      continue;

            if(c == '(')                                    round_cnt            ++;
            if(c == ')')                                    round_cnt            --;

            if(exception
            && round_last >= round_cnt
            &&
            (  c == ')'
            || c == ';'
            || c == '{'))
            {
                                                            skip_iteration       = true;
                                                            exception            = false;
            }

            if(!exception)
                if(!where_generic_state)
                    if(new_code.length() >= LEN_BUFFER
                    && check_exceptions(buffer))
                    {
                                                            round_last           = round_cnt;
                                                            exception            = true;
                                                            brackets_round_close = false;
                        if(c == '(')                        round_last           --;
                    }

            if(c == '{')
            {
                                                            where_generic_state  = false;
                                                            brackets_figure_open = true;
            }
            else                                            brackets_figure_open = false;

            if(c == ':')
                if(brackets_round_close)
                    if(  !where_generic_state
                    && is_where_generic(new_code))          where_generic_state  = true;

            if(brackets_round_close)
            {
                if(!where_generic_state)
                    if(c == '>')                            brackets_round_close = false;

                if(c == ';'
                || c == ']'
                || c == '}')
                {
                                                            where_generic_state  = false;
                                                            brackets_round_close = false;
                }
            }

            if(c == ')')
                if(!skip_iteration
                && !exception)                              brackets_round_close = true;

            if( brackets_round_close
            &&  brackets_figure_open)
            {
                brackets_round_close = false;

                insert = get_insert(file_path, lines, inserts ++);

                new_code += insert;
                lines += (get_count_lines(insert));
            }

            skip_iteration = false;
        }

        if(inserts)
        {
            fclose(f);
            remove(file_path.c_str());

            ofstream fout(file_path, ios::out);

            if(!fout.is_open())
            {
                perror("ofstream.is_open");
                system("pause");
                exit  (1);
            }

            fout << new_code;

            fout.close();

            return;
        }

        fclose(f);
    }
}

void offset_string_left(char str[], int len, int offset)
{
    for(int i = 0; i < offset; ++ i)        for(int s = 0; s < len; ++ s)       str[s] = str[s + 1];

    str[len - 1] = '\0';
}

bool is_close_quote(string code)
{
    int slash = 0;

    int pre_last_ID = code.length() - 2;
    for(int i = pre_last_ID; i >= 0; -- i)
    {
        if(code[i] != '\\')                 break;
        
        ++ slash;
    }

    return !(slash % 2);
}

// ------------------------------------------------------------------------------------------------------------------------------------------------------------

    const int NUM           = 11,
              LEN_EXCEPTION =  9;
    char list_words_exceptions[NUM][LEN_EXCEPTION];
    int  list_lens_exceptions[NUM];

void init_check_exceptions()
{
    int i = 0;
    for(int cell = 0; cell < NUM; ++ cell)          for(i = 0; i < LEN_EXCEPTION; ++ i)         list_words_exceptions[cell][i] = '\0';

    int d = 0;
    init_str(list_words_exceptions[d], "if"       , LEN_EXCEPTION, list_lens_exceptions[d] = 2); ++ d; //  1
    init_str(list_words_exceptions[d], "for"      , LEN_EXCEPTION, list_lens_exceptions[d] = 3); ++ d; //  2
    init_str(list_words_exceptions[d], "new"      , LEN_EXCEPTION, list_lens_exceptions[d] = 3); ++ d; //  3
    init_str(list_words_exceptions[d], "lock"     , LEN_EXCEPTION, list_lens_exceptions[d] = 4); ++ d; //  4
    init_str(list_words_exceptions[d], "when"     , LEN_EXCEPTION, list_lens_exceptions[d] = 4); ++ d; //  5
    init_str(list_words_exceptions[d], "while"    , LEN_EXCEPTION, list_lens_exceptions[d] = 5); ++ d; //  6
    init_str(list_words_exceptions[d], "using"    , LEN_EXCEPTION, list_lens_exceptions[d] = 5); ++ d; //  7
    init_str(list_words_exceptions[d], "catch"    , LEN_EXCEPTION, list_lens_exceptions[d] = 5); ++ d; //  8
    init_str(list_words_exceptions[d], "switch"   , LEN_EXCEPTION, list_lens_exceptions[d] = 6); ++ d; //  9
    init_str(list_words_exceptions[d], "foreach"  , LEN_EXCEPTION, list_lens_exceptions[d] = 7); ++ d; // 10
    init_str(list_words_exceptions[d], "delegate" , LEN_EXCEPTION, list_lens_exceptions[d] = 8);       // 11
}

int check_exceptions(const char str[])
{
    int len_str2   = 0,
        start_str1 = 0;

    bool true_begin = false,
         true_end   = false;

    for(int cell = 0; cell < NUM; ++ cell)
    {
        // len_str2   = get_len(list_words_exceptions[cell]);
        len_str2 = list_lens_exceptions[cell];
        start_str1 = LEN_BUFFER - 1 - len_str2;

        if(is_equali(str, list_words_exceptions[cell], LEN_BUFFER, len_str2, start_str1, 0))
        {
            true_begin = false;
            true_end   = false;

            switch(str[start_str1 - 1])
            {
                case '\\' : {true_begin = true; break;}
                case '='  : {true_begin = true; break;}
                case ','  : {true_begin = true; break;}
                case ';'  : {true_begin = true; break;}
                case '{'  : {true_begin = true; break;}
                case '}'  : {true_begin = true; break;}
                case ' '  : {true_begin = true; break;}
                case '\t' : {true_begin = true; break;}
                case '\n' : {true_begin = true; break;}
                case '('  : {true_begin = true; break;}
                case ')'  : {true_begin = true; break;}
            }

            switch(str[LEN_BUFFER - 1])
            {
                case ' '  : {true_end = true; break;}
                case '\t' : {true_end = true; break;}
                case '\n' : {true_end = true; break;}
                case '('  : {true_end = true; break;}
            }

            if(true_begin
            && true_end)        return cell + 1;
        }
    }

    return 0;
}

bool is_where_generic(string new_code)
{
    int stage = 1;
    int  len_new_code = new_code.length();

    const int LEN = 5;
    char sym,
         word_search[LEN + 1],
         word_check [LEN + 1];

    word_search[LEN] = '\0';
    word_check [LEN] = '\0';

    init_str(word_search, "where" , LEN, LEN);

    for(int i = len_new_code - 2; i >= 0; -- i)
    {
        sym = new_code[i];

        switch(stage)
        {
            case 1:
            {
                if(sym != ' '
                && sym != '\t'
                && sym != '\n')
                    stage = 2;

                break;
            }
            case 2:
            {
                if(sym == ' '
                || sym == '\t'
                || sym == '\n')
                    stage = 3;

                break;
            }
            case 3:
            {
                if(sym != ' '
                && sym != '\t'
                && sym != '\n')
                {
                    if(i <  LEN)                                                return false;

                    for(int d  = LEN - 1;
                           (d >= 0)
                        && (i >= 0);
                            -- d,
                            -- i)       word_check[d] = new_code[i];

                    ++ i;

                    if(!is_equali(word_check, word_search, LEN, LEN, 0, 0))     return false;

                    stage = 4;
                }

                break;
            }
            case 4:
            {
                if(sym == ')')                                                      return true;
                else
                    if(sym != ' '
                    && sym != '\t'
                    && sym != '\n')                                             return false;

                break;
            }
        }
    }

                                                                                return false;
}

string get_insert(string file_path, int line, int inserts)
{
    return (string)"\n" +
"           Project.Common.MethodsOperationLog.log_to_buffer(\"" +  file_path  + "\", " + to_string(line) + ", " + to_string(inserts) + ", 2);\n";
}

int get_count_lines(string insert)
{
    int result = 0;

    int len = insert.length();
    for(int c = 0; c < len; ++ c)       if(insert[c] == '\n')       result ++;

    return result;
}

// ------------------------------------------------------------------------------------------------------------------------------------------------------------

void init_str(char str1[], const char str2[], int len_str1, int len_str2)
{
    for(int     i = 0;
               (i < len_str1)
            && (i < len_str2);
            ++  i)                  str1[i] = str2[i];
}

// Вернет длину строки.
int get_len(const char str[])
{
    int result = 0;

    while(str[result])      ++ result;

    return result;
}

bool is_equali(const char str1[], const char str2[], const int len_str1, const int len_str2, int start_str1, int start_str2)
{
    for(;
               (start_str1 < len_str1)
            && (start_str2 < len_str2);
                ++ start_str1,
                ++ start_str2)                  if(to_lower(str1[start_str1]) != to_lower(str2[start_str2]))        return false;

    return true;
}

// Перевод символа в нижний регистр.
char to_lower(char ch)
{
    if(64 < ch < 91)        ch += 32;

    return ch;
}
